print('Hello, World!')
print(42)

# using a variable
number = 33
print('number:', number)

# 2 command in one line separeted by a semi colon
sayHelloToUser = 'Hello, User!'; print(sayHelloToUser)

# user input 1
message = input('Type a message to yourself: ')
print('You said: ', message)

# user input 2
num = int(input('Type a number: '))
print('You entered: ', num)

# challenge: user input 3
firstName = input('Please enter your first name: ')
lastName = input('Please enter your last name: ')
print('Full name: ', firstName, lastName)